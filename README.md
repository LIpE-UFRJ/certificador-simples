# Certificador Simples

Um programa opensource que gera e envia certificados em formato PDF de forma automatizada, senguindo modelos customizáveis e a partir de uma base de dados fornecida pelo usuário.

Para saber mais, acesse a documentação ou o repositório de código fonte.

- Documentação: [https://certificador-simples.readthedocs.io](https://certificador-simples.readthedocs.io)
- Código-fonte: [https://gitlab.com/renanrms/certificador-simples](https://gitlab.com/renanrms/certificador-simples)
