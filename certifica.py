#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Imports da biblioteca padrão
import argparse
import json
import os
import sys

# Imports locais
import module_functions

# Definições padrão de variáveis.
program_dir = os.path.dirname(os.path.abspath(__file__))

help_description='''\
Um programa opensource que gera e envia certificados em formato PDF de forma
automatizada, senguindo modelos customizáveis e a partir de uma base de dados
fornecida pelo usuário.'''
help_epilog='''\
Para saber mais, acesse a documentação ou o repositório de código fonte.
    Documentação: https://certificador-simples.readthedocs.io
    Código-fonte: https://gitlab.com/LIpE-UFRJ/certificador-simples'''

# Pega opções de linha de comando com dados sobre o local do projeto e etc.
parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter, 
    description=help_description, 
    epilog=help_epilog
    )

parser.add_argument('-g', '--generate', metavar='PROJECT', help='Gera os certificados referentes ao projeto indicado.')
parser.add_argument('-s', '--send',     metavar='PROJECT', help='Envia os certificados gerados previamente.')
parser.add_argument('-E', '--example',  metavar='MODEL',   help='Exporta o exemplo de projeto referente ao modelo indicado para a pasta atual do usuário.')
parser.add_argument('-e', '--export',   metavar='MODEL',   help='Exporta o modelo indicado para a pasta atual do usuário.')
parser.add_argument('-i', '--import',   metavar='MODEL',   help='Importa o modelo indicado para dentro do software.', dest='importe')
parser.parse_args()

# Executa as operações do programa.
if (parser.parse_args().generate != None):
    configs = module_functions.load_project_configs(os.path.realpath(parser.parse_args().generate), program_dir)
    module_functions.generate_certificates(configs['project_path'], configs['model_path'])

if (parser.parse_args().send != None):
    configs = module_functions.load_project_configs(os.path.realpath(parser.parse_args().send), program_dir)
    service = module_functions.login_gmail()
    module_functions.send_certificates(service, configs['project_path'], configs['copy_address'])

if (parser.parse_args().example != None):
    module_functions.export_example(parser.parse_args().example, program_dir)

if (parser.parse_args().export != None):
    module_functions.export_model(parser.parse_args().export, program_dir)

if (parser.parse_args().importe != None):
    module_functions.import_model(parser.parse_args().importe, program_dir)
