Certificador Simples
==============================================
Certificador Simples é um programa opensource que gera e envia certificados em formato PDF de forma automatizada, senguindo modelos customizáveis e a partir de uma base de dados fornecida pelo usuário. A geração dos certificados ocorre a partir de um arquivo CSV que contém os dados pessoais de cada certificado e de um conjunto de arquivos que definem o texto dos certificados e dos e-mails que serão enviados. O envio é feito através da API do Google, autenticando-se em uma conta Gmail no momento do envio.


Instalação
==============================================
#. Verifique se o python3 está instalado com o comando ``python3 --version`` no terminal. Se o comando exibir a versão na tela pule para o próximo passo. Caso contrário, busque instalar o Python3 na central de programas do seu sistema operacional ou busque na internet.

#. Baixe o programa `clicando aqui <https://gitlab.com/LIpE-UFRJ/certificador-simples/-/archive/master/certificador-simples-master.tar>`_, ou clonando o repositório.

#. Descompacte o arquivo baixado com o comando ``tar -xvf certificador-simples-master.tar``, ou utilize a ferramenta gráfica do seu sistema para fazê-lo.

#. Abra o terminal na pasta do programa e execute o script de instalação com permissão de super-usuário através do comando ``sudo python3 setup.py -i``.

Após isto, tudo deve estar instalado. Teste o programa executando no terminal ``certifica --help``.


Primeiros Passos
==============================================
Antes de começar é importante entender dois conceitos: o de **modelo de certificado** e o de **projeto de certificado**.

* Os **modelos** definem a aparência dos certificados, a sua diagramação e formatação em geral. São conjuntos de arquivos escritos em **HTML5** e **CSS3**, que definem a forma dos certificados, sem conter nenhum texto ou imagem do certificado. Estes arquivos vêm no próprio software e podem ser alterados pelo usuário, para gerar modelos diferentes, mas são flexíveis o suficiete para que sejam usados para diferentes projetos sem alteração.
* Já os **projetos** definem o conteúdo de um conjunto de certificados (como de um curso específico) sem conter aspectos de forma e diagramação. Em geral são constituídos de uma pasta com um conjunto de arquivos com nomes específicos, que são definidos pelo modelo adotado. Este é elaborado pelo usuário.

Assim para iniciar o seu projeto exporte o projeto de exemplo de algum modelo com o comando ``certifica -E default``. Assim, o exemplo de projeto para o modelo padrão (*"default"*) será adicionado à sua pasta atual. Você pode indicar outro modelo conhecendo seu nome.

Para fazer um primeiro teste, use o comando ``certifica -g example``, que deverá gerar os certificados para o exemplo. Verifique o conteúdo na pasta ``outputs`` e compare o contúdo dos certificados com os arquivos do projeto. Depois disso, você poderá alterá-los facilmente para gerar os certificados que quiser. Perceba apenas que os dados específicos de cada certificado devem estar em um arquivo ``dados.csv``.

Depois altere este arquivo com um editor de texto para conter um e-mail real (pode ser o seu para um pequeno teste). Então utilize o comando ``certifica -s example`` para enviar os certificados gerados anteirormente. Nesse passo, você deverá logar em uma conta Gmail a que tenha acesso e os certificados serão enviados a partir desta. Confira seus e-mails enviados para ver o resultado.


Documentação do código-fonte
==============================================


Licença
==============================================


Índices e tabelas
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
