#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Imports da biblioteca padrão
import argparse
import os
import sys

# Definições padrão de variáveis.
program_dir = os.path.dirname(os.path.abspath(__file__))

help_description='''\
Programa de instalação do Certificador Simples.'''
help_epilog='''\
Para saber mais, acesse a documentação ou o repositório de código fonte.
    Documentação: https://certificador-simples.readthedocs.io
    Código-fonte: https://gitlab.com/LIpE-UFRJ/certificador-simples'''

# Pega opções de linha de comando com dados sobre o local do projeto e etc.
parser = argparse.ArgumentParser(
    formatter_class=argparse.RawDescriptionHelpFormatter, 
    description=help_description, 
    epilog=help_epilog
    )

parser.add_argument('-i', '--install',   metavar='DIRECTORY', nargs='?', const=True, help='Instala o programa no local informado (Padrão: /usr/share). Independente do diretório, será criado um link em /usr/bin. Também pode ser usado para atualização do programa.')
parser.add_argument('-u', '--uninstall', action='store_true',                          help='Desinstala o programa do sistema.')
parser.parse_args()


if (os.geteuid() != 0):
    print('Este é um script de instalação que precisa ser chamado com privilégios de super-usuário. Tente novamente adicionando "sudo " ao início do comando. A senha será pedida para prosseguir.')
    exit()

def install_dependencies():
    if (os.system('apt version') == 0):
        os.system('apt update')
        os.system('apt install -y python3-pip')
        os.system('apt install -y poppler-utils')
    elif (os.system('pacman --version') == 0):
        print('\033[31m' + '\nScript não implementado completamente para este sistema. Instale estas dependências manualmente: python3-pip poppler-utils\n' + '\033[0m')
    elif (os.system('dnf --version') == 0):
        print('\033[31m' + '\nScript não implementado completamente para este sistema. Instale estas dependências manualmente: python3-pip poppler-utils\n' + '\033[0m')
    
    os.system('pip3 install -r python-requirements.txt')

def uninstall():
    link_path = '/usr/bin/certifica'
    if (not os.path.lexists(link_path)):
        print('O programa já não está instalado.')
        exit()
    folder_path = os.path.dirname(os.path.realpath(link_path))
    os.system(f'rm -r "{folder_path}"')
    os.system(f'rm "{link_path}"')

def install(directory):
    install_dependencies()
    directory = os.path.realpath(directory)
    if (f'{directory}/certificador-simples' == program_dir):
        print('Erro: O programa já está rodando no diretório alvo.')
        exit()
    if (os.path.lexists('/usr/bin/certifica')):
        uninstall()
    if (os.path.exists(f'{directory}/certificador-simples')):
        os.system(f'rm -r "{directory}/certificador-simples"')
    os.system(f'mkdir "{directory}/certificador-simples"')
    os.system(f'cp -r "{program_dir}" "{directory}/"')
    os.system(f'ln -s "{directory}/certificador-simples/certifica.py" /usr/bin/certifica')

# Executa as operações do programa.

print(parser.parse_args().install)
if (parser.parse_args().install):
    if (parser.parse_args().install == True):
        install('/usr/share')
    else:
        install(parser.parse_args().install)

print(parser.parse_args().uninstall)
if (parser.parse_args().uninstall):
    uninstall()
