#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from __future__ import print_function

# Imports gerais
import csv
import json
import os.path
import pickle
import re

# Imports para lidar com o envio de e-mails
import base64
import email.encoders
import mimetypes
from google.auth.transport.requests import Request
from google_auth_oauthlib.flow import InstalledAppFlow
from googleapiclient.discovery import build
from apiclient import errors
from email.mime.audio import MIMEAudio
from email.mime.base import MIMEBase
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

# Retira redundâncias da lista de strings e retira os caracteres que marcam as strings especiais no início e no fim de cada string.
def clear_strings(string_list):
    temp = []
    for item in string_list:
        clear_string = str.strip(item[1:-1])
        if (not clear_string in temp):
            temp.append(clear_string)
    return temp

# Conta a quantidade de elementos de um objeto iterável por loop for.
def length(iterable):
    count = 0
    for item in iterable:
        del(item)
        count += 1
    return count

# Carrega as configurações do projeto a partir de sua pasta.
def load_project_configs(project_path, program_dir):

    # Carrega a configuração padrão e atualiza com a do projeto se existir.
    configs = {}
    if (os.path.exists(f'{project_path}/configs.json')):
        with open(f'{project_path}/configs.json', 'r') as file:
            configs = json.loads(file.read())
    with open(f'project-configs-default.json', 'r') as file:
        default = json.loads(file.read())
    default.update(configs)
    configs = default

    configs['project_path'] = project_path

    # Tratamento do path da pasta de modelos.
    if (configs['model_path'][0] == '/'):
        pass
    if (configs['model_path'][0:2] == './'):
        configs['model_path'] = f'{configs["project_path"]}/{configs["model_path"][2:]}'
    else:
        configs['model_path'] = f'{program_dir}/models/{configs["model_path"]}'
    configs['model_path'] = os.path.realpath(configs['model_path'])

    return configs

# Carrega as strings especiais que estejam nos arquivos JSON já previstos.
def load_main_special_strings(project_path):
    main = {}

    # Carrega os dados individualizados de cada certificado em uma lista de JSONs.
    individual_data = []
    with open(f'{project_path}/dados.csv', 'r') as csvfile:
        csv_reader = csv.DictReader(csvfile)
        for data in csv_reader:
            individual_data.append(data)

    main['repetition_index'] = None

    return (main, individual_data)
    
# Processa uma string especial.
def process_special_string(special_string, main, project_path):
    special = json.loads(special_string)

    if (not 'src' in special.keys()):
        special['src'] = None
    if (not 'code' in special.keys()):
        special['code'] = None
    if (not 'var' in special.keys()):
        special['var'] = None
    if (not 'repetition_target' in special.keys()):
        special['repetition_target'] = None

    # Tratamento de erro.
    if ((special['src']!=None and special['code']!=None) or
        (special['code']!=None and special['var']!=None) or
        (special['var']!=None and special['src']!=None)):
        print ('\nErro: Apenas um entre os campos "src", "code" e "var" deve ser utilizado em uma string especial.')
        exit()

    # Obtém o conteúdo principal
    if (special['src'] != None):
        with open(f'{project_path}/{special["src"]}') as file:
            containing = file.read()

    if (special['code'] != None):
        containing = special['code']

    if (special['var'] != None):
        containing = eval(special['var'], {}, {'main': main})
    
    # Trata a repetição e retorna o conteúdo.
    if (special['repetition_target'] != None):

        # Separa a extensão do nome do arquivo.
        string_list = special['repetition_target'].rsplit('.', 1)
        file_name = string_list[0]
        if (len(string_list) > 1):
            file_ext = '.' + string_list[1]
        else:
            file_ext = ''

        # Gera o conteúdo da saída para cada arquivo encontrado na sequência.
        result = ""
        count = 1
        while (os.path.exists(f'{project_path}/{file_name}{count}{file_ext}')):
            main['repetition_index'] = count
            result += process_source(containing, main, project_path).replace(r'{n}', f'{count}')
            count += 1
        main['repetition_index'] = None
    else:
        result = process_source(containing, main, project_path)

    return result

# Processa um texto (uma string) que possa conter strings especiais, substituindo seus resultados.
def process_source(source, main, project_path):
    prefix = '<!--{'    # início de uma string especial 
    posfix = '}-->'    # fim de uma string especial
    current = 0
    depth = 0
    old_depth = 0
    result = ''
    special_string = ''

    while (True):

        # Verifica se a posição atual inicia o prefixo.
        if (current+len(prefix) <= len(source) and source[current:current+len(prefix)] == prefix):
            depth += 1

        # Verifica se a posição atual ocorre após um posfixo.
        if (current <= len(source) and source[current-len(posfix):current] == posfix):
            depth -= 1
            # Se estamos em um nível negativo, algo deu errado...
            if (depth < 0):
                print ('\nErro: Parece que um escopo de uma string especial foi fechado no modelo sem ser aberto antes...')
                exit()

        # Se a string especial terminou, esta é processada e a saída adicionada ao resultado.
        if (old_depth == 1 and depth == 0):
            special_string = special_string[len(prefix)-1:len(special_string)-len(posfix)+1]
            result += process_special_string(special_string, main, project_path)
            special_string = ''

        # Se o texto fonte terminou, retona-se o resultado.
        if (current >= len(source)):
            return result

        # Se estamos em um escopo de nível zero, o caractere é adicionado diretamente ao resultado.
        if (depth == 0):
            result += source[current]
        # Se estamos dentro de uma string especial o caractere é guardado para processamento ao final da string.
        elif (depth > 0):
            special_string += source[current]

        old_depth = depth
        current += 1

def generate_certificates(project_path, model_path):
    # Pega o texto de cada arquivo html.
    with open(f'{model_path}/front_page.html') as front_page_file:
        front_page = front_page_file.read()
    with open(f'{model_path}/back_page.html') as back_page_file:
        back_page = back_page_file.read()

    (main, individual_data) = load_main_special_strings(project_path)

    # Copia o conteúdo do modelo (html/css) para a pasta do projeto.
    if (os.path.exists(f'{project_path}/.temp')):
        os.system(f'rm -r "{project_path}/.temp"')
    os.system(f'cp -r "{model_path}" "{project_path}/.temp"')
    if (os.path.exists(f'{project_path}/images')):
        os.system(f'cp -r "{project_path}/images" "{project_path}/.temp/images"')
    os.remove(f'{project_path}/.temp/front_page.html')
    os.remove(f'{project_path}/.temp/back_page.html')
    if (not os.path.exists(f'{project_path}/outputs')):
        os.mkdir(f'{project_path}/outputs' , 0o744 )
    if (os.path.exists(f'{project_path}/outputs/rendering_errors.txt')):
        os.remove(f'{project_path}/outputs/rendering_errors.txt')

    progress = 0
    for data in individual_data:
        main['data'] = data

        print(f'Gerando documento {progress+1}/{len(individual_data)}... ', end='', flush=True)

        # Processa os modelos substituindo as strings especiais contidas.
        front_page_temp = process_source(front_page, main, project_path)
        back_page_temp = process_source(back_page, main, project_path)

        # Grava o html nos arquivos.
        front_page_file = open(f'{project_path}/.temp/front_page.html', 'w')
        front_page_file.write(front_page_temp)
        front_page_file.close()

        back_page_file = open(f'{project_path}/.temp/back_page.html', 'w')
        back_page_file.write(back_page_temp)
        back_page_file.close()

        # Gera o PDF na pasta outputs.
        os.system(f'weasyprint 2>>"{project_path}/outputs/rendering_errors.txt" "{project_path}/.temp/front_page.html" "{project_path}/.temp/front_page.pdf"')
        os.system(f'weasyprint 2>>"{project_path}/outputs/rendering_errors.txt" "{project_path}/.temp/back_page.html" "{project_path}/.temp/back_page.pdf"')
        os.system(f'pdfunite "{project_path}/.temp/front_page.pdf" "{project_path}/.temp/back_page.pdf" "{project_path}/outputs/{data["nome"]}.pdf"')
        os.remove(f'{project_path}/.temp/front_page.html')
        os.remove(f'{project_path}/.temp/back_page.html')

        print('Concluído.')
        progress += 1

    os.system(f'rm -r "{project_path}/.temp"')

    # Verifica se houve warnings na rederização do weasyprint avisa o usuário quando houver.
    with open(f'{project_path}/outputs/rendering_errors.txt') as error_file:
        if (error_file.read() == ''):
            os.remove(f'{project_path}/outputs/rendering_errors.txt')
        else:
            print(f'Houve erros ou warnings na renderização dos arquivos PDF. As mensagens do renderizador estão salvas em "{project_path}/outputs/rendering_errors.txt" .')

# Exporta o exemplo de projeto de um modelo indicado do programa para a pasta do usuário.
def export_example(model, program_dir):
    example_path = os.path.realpath(f'{program_dir}/models/{model}/example')
    os.system(f'cp -r "{example_path}" ./')

# Exporta um modelo indicado do programa para a pasta do usuário.
def export_model(model, program_dir):
    model_path = os.path.realpath(f'{program_dir}/models/{model}')
    os.system(f'cp -r "{model_path}" ./')

# Importa um modelo indicado para dentro do programa.
def import_model(model_path, program_dir):
    os.system(f'cp -r "{model_path}" "{program_dir}/models/"')

def login_gmail():
    # If modifying these scopes, delete the file token.pickle.
    SCOPES = ['https://www.googleapis.com/auth/gmail.send']

    flow = InstalledAppFlow.from_client_secrets_file('credentials.json', SCOPES)
    creds = flow.run_local_server(port=0)

    service = build('gmail', 'v1', credentials=creds)

    print('')

    return service

def create_message_with_attachment(
        sender, to, subject, message_text, file):
    """Create a message for an email.

    Args:
    sender: Email address of the sender.
    to: Email address of the receiver.
    subject: The subject of the email message.
    message_text: The text of the email message.
    file: The path to the file to be attached.

    Returns:
    An object containing a base64url encoded email object.
    """
    message = MIMEMultipart()
    message['to'] = to
    message['from'] = sender
    message['subject'] = subject

    msg = MIMEText(message_text)
    message.attach(msg)

    content_type, encoding = mimetypes.guess_type(file)

    if content_type is None or encoding is not None:
        content_type = 'application/octet-stream'
    main_type, sub_type = content_type.split('/', 1)

    if main_type == 'text':
        fp = open(file, 'rb')
        msg = MIMEText(fp.read().decode("utf-8"), _subtype=sub_type)
        fp.close()
    elif main_type == 'image':
        fp = open(file, 'rb')
        msg = MIMEImage(fp.read(), _subtype=sub_type)
        fp.close()
    elif main_type == 'audio':
        fp = open(file, 'rb')
        msg = MIMEAudio(fp.read(), _subtype=sub_type)
        fp.close()
    else:
        fp = open(file, 'rb')
        msg = MIMEBase(main_type, sub_type)
        msg.set_payload(fp.read())
        fp.close()
    filename = os.path.basename(file)
    msg.add_header('Content-Disposition', 'attachment', filename=filename)
    email.encoders.encode_base64(msg)
    message.attach(msg)

    raw_message = base64.urlsafe_b64encode(message.as_string().encode("utf-8"))

    return {'raw': raw_message.decode("utf-8")}

def send_message(service, user_id, message):
    """Send an email message.

    Args:
        service: Authorized Gmail API service instance.
        user_id: User's email address. The special value "me"
        can be used to indicate the authenticated user.
        message: Message to be sent.

    Returns:
        Sent Message.
    """
    try:
        message = (service.users().messages().send(userId=user_id, body=message).execute())
        # print (f'Message Id: {message["id"]}')
        return message
    except errors.HttpError as error:
        print (f'An error occurred: {error}')

def send_certificate(service, project_path, main, copy_address):
    with open(f'{project_path}/e-mail/assunto.txt') as text_file:
        containing = text_file.read()
        subject = process_source(containing, main, project_path)

    with open(f'{project_path}/e-mail/conteudo.txt') as text_file:
        containing = text_file.read()
        message_text = process_source(containing, main, project_path)

    if (copy_address != ""):
        copy_address = ", " + copy_address

    message = create_message_with_attachment(
        'me',
        main["data"]["e-mail"] + copy_address, 
        subject, 
        message_text, 
        f'{project_path}/outputs/{main["data"]["nome"]}.pdf')

    send_message(service, "me", message)

def send_certificates(service, project_path, copy_address):
    (main, individual_data) = load_main_special_strings(project_path)

    progress = 0
    for data in individual_data:
        main['data'] = data

        print(f'Enviando documento {progress+1}/{len(individual_data)}... ', end='', flush=True)

        send_certificate(service, project_path, main, copy_address)

        print('Concluído.')
        progress += 1

